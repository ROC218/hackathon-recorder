import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet({ "/controller"})
public class Controller extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	public Controller() {
		super();
	}
	
	public void init() {
	}
	
	protected void doGet(HttpServletRequest request, HttpServletRequest response) {
		String mode = response.getParameter("mode");
		switch (mode) {
		//TODO implement below cases
		case "vocal":
			System.out.println("processing " + mode);
			break;
		case "bass":
			System.out.println("processing " + mode);
			break;
		case "guitar":
			System.out.println("processing " + mode);
			break;
		case "drum":
			System.out.println("processing " + mode);
			break;
		case "combine":
			System.out.println("processing " + mode);
			break;
		case "play":
			System.out.println("processing " + mode);
			break;	
		}
	}
}
