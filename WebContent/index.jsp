<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Record - Combine - Play - for reference</title>
</head>
<body>
	<center>
		<table style="text-align: center">
			<tr>
				<td><input type="button" value="record vocal" onClick="location.href='controller?mode=vocal'" /></td>
			</tr>
			<tr>
				<td><input type="button" value="record bass" onClick="location.href='controller?mode=bass'" /></td>
			</tr>
			<tr>
				<td><input type="button" value="record guitar" onClick="location.href='controller?mode=guitar'" /></td>
			</tr>
			<tr>
				<td><input type="button" value="record drum" onClick="location.href='controller?mode=drum'" /></td>
			</tr>
			<tr>
				<td><input type="button" value="combine" onClick="location.href='controller?mode=combine'" /></td>
			</tr>
			<tr>
				<td><input type="button" value="play" onClick="location.href='controller?mode=play'" /></td>
			</tr>
		</table>
	</center>
</body>
</html>